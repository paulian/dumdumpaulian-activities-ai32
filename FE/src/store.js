import Vuex from 'vuex';
import Vue from 'vue';
import axios from 'axios';

Vue.use(Vuex, axios);

export default new Vuex.Store({
    state: {
        bookresponse: [],
        patronresponse: [],
        categories: [],
        category: {
            id: '',
            category: ''
        },
        books: [],
        book: {
            id: '',
            name: '',
            author: '',
            copies: '',
            category_id: '',
        },
        patrons: [],
        patron: {
            id: '',
            last_name: '',
            first_name: '',
            middle_name: '',
            email: '',
            created_at: '',
        }, 
    },
    getters: {

    },
    actions: {
        fetchCategories({ commit }) {
            axios.get('/api/category').then((response) => {
                this.categories = response.data
                console.log(response.data)
                commit('setCategories', response.data)
            })
        },
        fetchBooks({ commit }) {
            axios.get('/api/books').then((response) => {
                this.books = response.data
                console.log(response.data)
                commit('setBooks', response.data)
            })
        },
        borrowBook({ commit }, borrow) {
            axios.get('/api/patronsfind/'+borrow.patronid).then((response) => {
                this.state.patronresponse = response.data;
                console.log(this.state.patronresponse)
            })
            axios.get('/api/booksfind/'+borrow.bookid).then((response) => {
                this.state.bookresponse = response.data;
                if(borrow.copies <= this.state.bookresponse.copies) {
                    this.state.bookresponse.copies = parseInt(this.state.bookresponse.copies) - parseInt(borrow.copies);
                    commit('setBorrow', borrow.copies)
                }else{
                    /**
                     * Error Message input copies is greater than the available book copies
                     */
                }
            })
            
        },
        returnBook({ commit }, borrow) {
            axios.get('/api/patronsfind/'+borrow.patronid).then((response) => {
                this.state.patronresponse = response.data;
                console.log(this.state.patronresponse)
            })
            axios.get('/api/booksfind/'+borrow.bookid).then((response) => {
                this.state.bookresponse = response.data;
                    this.state.bookresponse.copies = parseInt(this.state.bookresponse.copies) + parseInt(borrow.copies);
                    commit('setReturn', borrow.copies)
            }) 
        },
        addBook({ commit }, input) {
            axios.post('/api/booksinsert', {
                name: input.name,
                author: input.author,
                copies: input.copies,
                category_id: input.category_id,
                completed: false
            })
            commit('newBooks')
        },
        deleteBook({ commit }, id) {
            axios.delete('/api/booksdelete/'+id)
            commit('removeBooks', id)
        },
        editBook({ commit }, input) {
            axios.put('/api/booksupdate/'+input.editID, {
                name: input.name,
                author: input.author,
                copies: input.copies,
                category_id: input.category_id,
                completed: false
            }).then((response) => {
                this.books = response.data
                console.log(response.data)
                commit('updateBooks', input)
            })
        },
        fetchPatrons({ commit }) {
            axios.get('/api/patrons').then((response) => {
                this.patrons = response.data
                console.log(response.data)
                commit('setPatrons', response.data)
            })
        },
        addPatron({ commit }, input) {
            axios.post('/api/patronsinsert', {
                last_name: input.last_name,
                first_name: input.first_name,
                middle_name: input.middle_name,
                email: input.email,
                completed: false
            })
            commit('newPatrons')
        },
        deletePatron({ commit }, id) {
            axios.delete('/api/patronsdelete/'+id)
            commit('removePatrons', id)
        },
        editPatron({ commit }, input) {
            axios.put('/api/patronsupdate/'+input.editID, {
                last_name: input.last_name,
                first_name: input.first_name,
                middle_name: input.middle_name,
                email: input.email,
                completed: false
            }).then((response) => {
                this.patron = response.data
                console.log(response.data)
                commit('updatePatrons', input)
            })
        }
    },
    mutations: {
        setBooks: (state, book) => {
            state.books = book;
        },
        setCategories: (state, category) => {
            state.categories = category;
        },
        removeBooks: (state, id) => {
            let book = state.books.filter(b => b.id != id)
            state.books = book;
        },
        newBooks(state){
            state.books.unshift({ ...state.book });
        },
        updateBooks(state, input){
            this.input = ({ ...state.books[input.editID] });
        },
        setPatrons: (state, patron) => {
            state.patrons = patron;
        },
        removePatrons: (state, id) => {
            let patron = state.patrons.filter(p => p.id != id)
            state.patrons = patron;
        },
        newPatrons(state){
            state.patrons.unshift({ ...state.patron });
        },
        updatePatrons(state, input){
            this.input = ({ ...state.patrons[input.editID] });
        },
        setBorrow(state, copies){
            axios.put('/api/borrowedbooksupdate/'+state.patronresponse.id, {
                patron_id: state.patronresponse.copies,
                copies: state.bookresponse.copies,
                book_id: state.bookresponse.id,
                completed: false
            })
            axios.put('/api/booksupdate/'+state.bookresponse.id, {
                name: state.bookresponse.name,
                author: state.bookresponse.author,
                copies: state.bookresponse.copies,
                category_id: state.bookresponse.category_id,
                completed: false
            })
            axios.post('/api/borrowedbooksinsert', {
                patron_id: state.patronresponse.id,
                copies: copies,
                book_id: state.bookresponse.id,
                completed: false
            }).then((response) => {
                console.log(response.data)
            })

        },
        setReturn(state, copies){
            
            axios.put('/api/booksupdate/'+state.bookresponse.id, {
                name: state.bookresponse.name,
                author: state.bookresponse.author,
                copies: state.bookresponse.copies,
                category_id: state.bookresponse.category_id,
                completed: false
            })
            axios.post('/api/returnedbooksinsert', {
                patron_id: state.patronresponse.id,
                copies: copies,
                book_id: state.bookresponse.id,
                completed: false
            }).then((response) => {
                console.log(response.data)
            })

        }
    },

});