<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class BooksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name = ['IT Elective','Science','Gain','Dagger Reborn','Tear of The Falls','Hidden Web'];
        $author = ['Jhon Dalton','Eagle Bird','Ellie Argon','Thrish Aeger','Renz Venus','Aaron Swagger'];
        $copies = ['45','34','65','25','290','33'];
        $category_id = ['1','3','3','2','1','4'];
        $counter = 0;
        while($counter < 5){
            DB::table('books')->insert([
            'name' => $name[$counter],
            'author' => $author[$counter],
            'copies' => $copies[$counter],
            'category_id' => $category_id[$counter],
            ]);
            $counter++;
        }
    }
}
