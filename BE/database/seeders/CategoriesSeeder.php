<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ['Drama','Action','Adventure','Thriller','Comedy','Fantasy'];
        $counter = 0;
        while($counter < 5){
            DB::table('categories')->insert([
            'category' => $categories[$counter]
            ]);
            $counter++;
        }
    }
}
