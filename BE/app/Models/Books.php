<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    use HasFactory;

    protected $table = 'books';
    protected $fillable = ['name', 'author', 'copies', 'category_id'];

    public function getBorrowedBooksRelation() {
        return $this->hasOne( related: 'App\Borrowed_Books', foreignKey: 'book_id', localKey: 'id' );
    }

    public function getCategoriesBTRelation() {
        return $this->belongsTo( related: 'App\Categories', foreignKey: 'category_id', localKey: 'id' );
    }
}
