<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Borrowed_Books extends Model
{
    use HasFactory;

    protected $table = 'borrowed_books';
    protected $fillable = ['patron_id', 'copies', 'book_id'];

    public function getBooksBTRelation() {
        return $this->belongsTo( related: 'App\Books', foreignKey: 'book_id', localKey: 'id' );
    }
}
