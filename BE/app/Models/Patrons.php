<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patrons extends Model
{
    use HasFactory;

    protected $table = 'patrons';
    protected $fillable = ['last_name', 'first_name', 'middle_name', 'email'];

    public function getReturnedBooksHORelation() {
        return $this->hasOne( related: 'App\Returned_Books', foreignKey: 'patron_id', localKey: 'id' );
    }
}
