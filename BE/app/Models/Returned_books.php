<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Returned_books extends Model
{
    use HasFactory;

    protected $table = 'returned_books';
    protected $fillable = ['book_id', 'copies', 'patron_id'];

    public function getPatronsBTRelation() {
        return $this->belongsTo( related: 'App\Patrons', foreignKey: 'patron_id', localKey: 'id' );
    }
}
