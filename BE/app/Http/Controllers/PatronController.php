<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Patrons;

class PatronController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patdata = Patrons::all();
        return response()->json($patdata);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $patdata = new Patrons;
        $patdata->last_name = $request->input('last_name');
        $patdata->first_name = $request->input('first_name');
        $patdata->middle_name = $request->input('middle_name');
        $patdata->email = $request->input('email');
        $patdata->save();
        return response()->json($patdata);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $patdata = Patrons::find($id);
        $patdata->last_name = $request->input('last_name');
        $patdata->first_name = $request->input('first_name');
        $patdata->middle_name = $request->input('middle_name');
        $patdata->email = $request->input('email');
        $patdata->save();
        return response()->json($patdata);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patdata = Patrons::find($id);
        $patdata->delete();
        return response()->json($patdata);
    }

    public function patronfind($id)
    {
        $patdata = Patrons::find($id);
        return response()->json($patdata);
    }
}
