<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Returned_books;
use App\Models\Borrowed_Books;

class ReturnedBooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rebodata = Returned_books::all();
        return response()->json($rebodata);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rebodata = new Returned_books;
        $rebodata->book_id = $request->input('book_id');
        $rebodata->copies = $request->input('copies');
        $rebodata->patron_id = $request->input('patron_id');
        $rebodata->save();
        return response()->json($rebodata);
    }

    public function show(Request $request, $id)
    {
        $borrowedBookCopies = Borrowed_Books::find($id);
        $bookCopies = $borrowedBookCopies->copies;
        $bookCopiesInput = $request->input('copies');

        if($bookCopies < $bookCopiesInput){
            return response()->json(['message' => 'Returning book exceed number of copies of book borrowed']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rebodata = Returned_books::find($id);
        $rebodata->book_id = $request->input('book_id');
        $rebodata->copies = $request->input('copies');
        $rebodata->patron_id = $request->input('patron_id');
        $rebodata->save();
        return response()->json($rebodata);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rebodata = Returned_books::find($id);
        $rebodata->delete();
        return response()->json($rebodata);
    }
}
