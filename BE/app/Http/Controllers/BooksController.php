<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Books;
use App\Models\Categories;
use Validator;

class BooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bodata = Books::all();
        return response()->json($bodata);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bodata = new Books;
        $bodata->name = $request->input('name');
        $bodata->author = $request->input('author');
        $bodata->copies = $request->input('copies');
        $bodata->category_id = $request->input('category_id');
        $bodata->save();
        return response()->json($bodata);
    }

    public function show(Request $request, $id)
    {
        $category = Categories::find($id);
        if($category){
            return response()->json($category);
        }
        else{
            return validate()->json(['message' => 'Book not found']);
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bodata = Books::find($id);
        $bodata->name = $request->input('name');
        $bodata->author = $request->input('author');
        $bodata->copies = $request->input('copies');
        $bodata->category_id = $request->input('category_id');
        $bodata->save();
        return response()->json($bodata);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bodata = Books::find($id);
        $bodata->delete();
        return response()->json($bodata);
    }

    public function bookfind($id)
    {
        $bodata = Books::find($id);
        return response()->json($bodata);
    }
}
