<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\BorrowBookRequest;
use App\Models\Borrowed_Books;
use App\Models\Books;

class BorrowedBooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bobodata = Borrowed_Books::all();
        return response()->json($bobodata);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BorrowBookRequest $request)
    {
        $bobodata = new Borrowed_Books;
        $bobodata->patron_id = $request->input('patron_id');
        $bobodata->copies = $request->input('copies');
        $bobodata->book_id = $request->input('book_id');
        $bobodata->save();
        return response()->json($bobodata);
    }

    public function show(Request $request, $id)
    {
        $books = Books::find($id);
        $bookCopies = $books->copies;
        $bookCopiesInput = $request->input('copies');

        if($bookCopies < $bookCopiesInput){
            return response()->json(['message' => 'Borrowing book exceed number of copies']);
        }
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BorrowBookRequest $request, $id)
    {
        $bobodata = Borrowed_Books::find($id);
        $bobodata->patron_id = $request->input('patron_id');
        $bobodata->copies = $request->input('copies');
        $bobodata->book_id = $request->input('book_id');
        $bobodata->save();
        return response()->json($bobodata);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bobodata = Borrowed_Books::find($id);
        $bobodata->delete();
        return response()->json($bobodata);
    }
}
